This application is a simple demonstration of using the DMX973B as a
4 line x 16 character wide display. In this mode a look-up table is used
to decode the ascii character.  The font is fixed width, therefore some
words which containing letters like "i" will have too much space.  See the
proportional width font download for a solution.

The Software was generated using Hi-Tec PICC v8.00PL3. The checksum of the code is C4F5.

