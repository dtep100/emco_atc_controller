#include "CppUTest/TestHarness.h"
#include "types.h"
#include "E6CP-AG5C_encoder_driver.h"

extern "C"
{
	/*
	 * You can add your c-only include files here
	 */
}

TEST_GROUP(Encoder)
{
    void setup()
    {
    }

    void teardown()
    {
    }
};

TEST(Encoder, CanDecodeGrayToBinary)
{
  uint8_t test_binary_position;

  test_binary_position = convert_encoder_format_to_binary_position(0x00u);
	CHECK_EQUAL(0x00, test_binary_position);

  test_binary_position = convert_encoder_format_to_binary_position(0x01u);
	CHECK_EQUAL(0x01, test_binary_position);

  test_binary_position = convert_encoder_format_to_binary_position(0x03u);
	CHECK_EQUAL(0x02, test_binary_position);

  test_binary_position = convert_encoder_format_to_binary_position(0x02u);
	CHECK_EQUAL(0x03, test_binary_position);

  test_binary_position = convert_encoder_format_to_binary_position(0x06u);
	CHECK_EQUAL(0x04, test_binary_position);

  test_binary_position = convert_encoder_format_to_binary_position(0x80u);
	CHECK_EQUAL(0xFFu, test_binary_position);
}