#include "CppUTest/TestHarness.h"
#include "types.h"
#include "board.h"
#include "toolchange_modecon.h"

extern "C"
{
	/*
	 * You can add your c-only include files here
	 */
}

TEST_GROUP(Toolchange_mode_controller)
{
    void setup()
    {
      modecon_init();
    }

    void teardown()
    {
    }
};



TEST(Toolchange_mode_controller, AutoModeEnterAndToolControlledUserRequestsIgnored)
{
  #define AUTO_TOOL_REQ  1u
  #define AUTO_TOOL_EXPECTED  (AUTO_TOOL_REQ - 1u)
  #define MANUAL_TOOL_REQ  2u

  /* Set Auto mode */
  modecon_set_automode(true);

  /* Set tool requests */
  modecon_set_hosttoolrequest(AUTO_TOOL_REQ);
  modecon_set_usertoolrequest(MANUAL_TOOL_REQ);

  /* Execute mode controller */
  modecon_run_toolselection();

  /* Check mode and tool request */
  CHECK_EQUAL(AUTO_TOOL_EXPECTED, modecon_get_tool_request());
	CHECK_EQUAL(AUTOMATIC, modecon_get_mode());

}


TEST(Toolchange_mode_controller, ManualModeEnterAndToolControlHostRequestsIgnored)
{
  #define AUTO_TOOL_REQ  1u
  #define MANUAL_TOOL_REQ  2u

  /* Set Auto mode */
  modecon_set_automode(false);

  /* Set tool requests */
  modecon_set_hosttoolrequest(AUTO_TOOL_REQ);
  modecon_set_usertoolrequest(MANUAL_TOOL_REQ);

  /* Execute mode controller */
  modecon_run_toolselection();

  /* Check mode and tool request */
  CHECK_EQUAL(MANUAL_TOOL_REQ, modecon_get_tool_request());
	CHECK_EQUAL(MANUAL, modecon_get_mode());

}


TEST(Toolchange_mode_controller, InitInManualMode)
{
  CHECK_EQUAL(MANUAL, modecon_get_mode());
}



TEST(Toolchange_mode_controller, TestModeEnterControlsMotorAndOverridesNormal)
{
  #define NORMAL_CONTROL  FORWARD
  #define TEST_CONTROL    REVERSE

  /* Set motor control signals */
  modecon_set_normalmotorcontrol(NORMAL_CONTROL);
  modecon_set_automode(true);

  /* Execute motor controller */
  modecon_run_motorselection();

  /* Check the normal mode controls are selected */
  CHECK_EQUAL(AUTOMATIC, modecon_get_mode());
  CHECK_EQUAL(NORMAL_CONTROL, modecon_get_motorcontrol());

  /* Put into test mode */
  modecon_set_testmode(true);
  modecon_set_testmotorcontrol(TEST_CONTROL);

  /* Execute motor controller */
  modecon_run_motorselection();

  /* Check in test mode and the normal mode controls are overriden */
  CHECK_EQUAL(TEST, modecon_get_mode());
  CHECK_EQUAL(TEST_CONTROL, modecon_get_motorcontrol());

  /* Release test mode */
  modecon_set_testmode(false);

  /* Execute motor controller */
  modecon_run_motorselection();

  /* Check back in normal mode and the normal mode controls are selected */
  CHECK_EQUAL(AUTOMATIC, modecon_get_mode());
  CHECK_EQUAL(NORMAL_CONTROL, modecon_get_motorcontrol());

}