#include "types.h"
#include "board.h"
#include "output.h"


void init_output(void)
{
  /* Motor relays */
  pinMode (MOTOR_POS_RELAY, OUTPUT);
  pinMode (MOTOR_NEG_RELAY, OUTPUT);
  pinMode (MOTOR_SPEED_RELAY, OUTPUT);
  pinMode (MOTOR_SPEED_RELAY, OUTPUT);

  /* LCD */
  init_lcd();

  return;
}

void update_display(toolchanger_mode_t toolchanger_mode,
                    uint8_t tool_number,
                    toolchange_fsm_state_t changer_state,
                    uint8_t last_host_cmd,
                    uint8_t host_tool_selection,
                    uint8_t user_tool_selection,
                    uint8_t encoder_value,
                    processing_motor_request_t motor_command)
{

  /* First line */
  lcd_set_cursor(0);
  {
    /* Tool number */
    {
      lcd_print_ascii_char('T');
      lcd_print_ascii_char('o');
      lcd_print_ascii_char('o');
      lcd_print_ascii_char('l');
      lcd_print_ascii_char('#');
      lcd_print_ascii_char(':');
      lcd_print_dec_char(tool_number, 1u);
    }

    lcd_print_ascii_char(' ');
    lcd_print_ascii_char(' ');
    lcd_print_ascii_char(' ');
    
    /* Auto/Manual/Test mode */
    if(AUTOMATIC == toolchanger_mode)
    {
      lcd_print_ascii_char('A');
      lcd_print_ascii_char('U');
      lcd_print_ascii_char('T');
      lcd_print_ascii_char('O');
    }
    else if(MANUAL == toolchanger_mode)
    {
      lcd_print_ascii_char('M');
      lcd_print_ascii_char('A');
      lcd_print_ascii_char('N');
      lcd_print_ascii_char('U');
      lcd_print_ascii_char('A');
      lcd_print_ascii_char('L');
    }
    else
    {
      lcd_print_ascii_char('T');
      lcd_print_ascii_char('E');
      lcd_print_ascii_char('S');
      lcd_print_ascii_char('T');
    }

  }

  /* Second line */
  lcd_set_cursor(1);
  {
    lcd_print_ascii_char('S');
    lcd_print_ascii_char('t');
    lcd_print_ascii_char('a');
    lcd_print_ascii_char('t');
    lcd_print_ascii_char('e');
    lcd_print_ascii_char(':');

    switch(changer_state)
    {
      case IDLE:
      {
        lcd_print_ascii_char('I');
        lcd_print_ascii_char('D');
        lcd_print_ascii_char('L');
        lcd_print_ascii_char('E');
      }break;

      case ERROR:
      {
        lcd_print_ascii_char('E');
        lcd_print_ascii_char('R');
        lcd_print_ascii_char('R');
        lcd_print_ascii_char('O');
        lcd_print_ascii_char('R');
      }break;

      default:
      {
        lcd_print_ascii_char('C');
        lcd_print_ascii_char('H');
        lcd_print_ascii_char('A');
        lcd_print_ascii_char('N');
        lcd_print_ascii_char('G');
        lcd_print_ascii_char('I');
        lcd_print_ascii_char('N');
        lcd_print_ascii_char('G');
      }break;
    }
    
  }

  /* Third line */
  lcd_set_cursor(2);
  {
    lcd_print_ascii_char('C');
    lcd_print_ascii_char('M');
    lcd_print_ascii_char('D');
    lcd_print_ascii_char(':');
    lcd_print_hex_char(last_host_cmd, 2u);
    lcd_print_ascii_char(' ');

    lcd_print_ascii_char('H');
    lcd_print_ascii_char('T');
    lcd_print_ascii_char(':');
    lcd_print_dec_char(host_tool_selection, 1u);
    lcd_print_ascii_char(' ');

    lcd_print_ascii_char('U');
    lcd_print_ascii_char('T');
    lcd_print_ascii_char(':');
    lcd_print_dec_char(user_tool_selection, 1u);
    
  }

  /* Fourth line */
  lcd_set_cursor(3);
  {
    lcd_print_ascii_char('E');
    lcd_print_ascii_char('N');
    lcd_print_ascii_char('C');
    lcd_print_ascii_char(':');
    lcd_print_dec_char(encoder_value, 3u);
    lcd_print_ascii_char(' ');

    lcd_print_ascii_char('M');
    lcd_print_ascii_char('O');
    lcd_print_ascii_char('T');
    lcd_print_ascii_char(':');

    if(motor_command == FORWARD)
    {
      lcd_print_ascii_char('F');
      lcd_print_ascii_char('W');
      lcd_print_ascii_char('D');
    }
    else if(motor_command == REVERSE)
    {
      lcd_print_ascii_char('R');
      lcd_print_ascii_char('E');
      lcd_print_ascii_char('V');
    }
    else if(motor_command == LOCK)
    {
      lcd_print_ascii_char('L');
      lcd_print_ascii_char('O');
      lcd_print_ascii_char('C');
      lcd_print_ascii_char('K');
    }
    else
    {
      lcd_print_ascii_char('O');
      lcd_print_ascii_char('F');
      lcd_print_ascii_char('F');
    }
    
  }
  
  return;
}

void update_motor_controls(processing_motor_request_t command)
{   
  switch(command)
  {
    case FORWARD :
    {
      digitalWrite(MOTOR_POS_RELAY, HIGH);  
      digitalWrite(MOTOR_NEG_RELAY, LOW); 
      digitalWrite(MOTOR_SPEED_RELAY, LOW); 
    }break;
    
    case REVERSE :
    {
      digitalWrite(MOTOR_POS_RELAY, LOW);  
      digitalWrite(MOTOR_NEG_RELAY, HIGH); 
      digitalWrite(MOTOR_SPEED_RELAY, LOW); 
    }break;
    
    case LOCK :
    {
      digitalWrite(MOTOR_POS_RELAY, LOW);  
      digitalWrite(MOTOR_NEG_RELAY, HIGH); 
      digitalWrite(MOTOR_SPEED_RELAY, HIGH); 
    }break;
    
    default :
    {
      digitalWrite(MOTOR_POS_RELAY, LOW);  
      digitalWrite(MOTOR_NEG_RELAY, LOW); 
      digitalWrite(MOTOR_SPEED_RELAY, LOW); 
    }break;
  }
  return;
}
