void set_tool_number_request(uint8_t requested_tool);
void set_encoder_position(uint8_t encoder_position);
void set_tool_encoder_position(uint8_t tool_number, uint8_t encoder_value);
processing_motor_request_t get_motor_drive_request(void);
void run_toolchanger_fsm(void);
void init_toolchange_fsm(void);
toolchange_fsm_state_t changerfsm_get_state(void);

uint8_t convert_encoder_position_to_tool_range(uint8_t encoder_position, uint8_t* position_within_range);
bool_t test_if_encoder_in_reversing_range_for_tool_selection(uint8_t tool_selection, uint8_t encoder_position);
bool_t test_if_encoder_in_locking_range_for_tool_selection(uint8_t tool_selection, uint8_t encoder_position);
