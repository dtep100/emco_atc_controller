#include "CppUTest/TestHarness.h"
#include "types.h"
#include "board.h"
#include "toolchange_fsm.h"


extern "C"
{
	/*
	 * You can add your c-only include files here
	 */
}



TEST_GROUP(Processing)
{
  #define TOOL_ENCODER_INTERVAL (ENCODER_RANGE/NUM_TOOLS)
  #define TOOL_LOCK_POSITION(tool_number)      (TOOL_ENCODER_INTERVAL * (tool_number))  
  #define TOOL_REVERSE_POSITION(tool_number)   ((TOOL_ENCODER_INTERVAL * (tool_number)) + (TOOL_ENCODER_INTERVAL/2))

    void setup()
    {
		  init_toolchange_fsm();

      /* Configure tool positions */
      for(uint8_t tool_idx = 0u; tool_idx < NUM_TOOLS; tool_idx++)
      {
        set_tool_encoder_position(tool_idx , TOOL_LOCK_POSITION(tool_idx));
      }
    }

    void teardown()
    {
    }

    void helper_set_run_check(uint8_t tool_no, uint8_t encoder_pos, processing_motor_request_t expected_motor_request)
    {
      processing_motor_request_t motor_request;

      /* set tool number */
	    set_tool_number_request(tool_no);
	    /* set encoder position */
	    set_encoder_position(encoder_pos);
	
	    /* execute processing */
	    run_toolchanger_fsm();

	    /* check driving forward */
	    motor_request = get_motor_drive_request();
	    CHECK_EQUAL(expected_motor_request, motor_request);
    }
};



IGNORE_TEST(Processing, ToleratesJammedChanger)
{ /* Test that the motor output is switch off if the encoder shows the stops moving or if the change takes too long */
  #define JAMMEDCHANGER_TARGET_TOOL 3u

  helper_set_run_check(JAMMEDCHANGER_TARGET_TOOL, TOOL_LOCK_POSITION(JAMMEDCHANGER_TARGET_TOOL - 1u), FORWARD);

  {
    uint8_t retry_idx = 100u;
    while(retry_idx--)
    {
      run_toolchanger_fsm();
    }
  }

  helper_set_run_check(JAMMEDCHANGER_TARGET_TOOL, TOOL_LOCK_POSITION(JAMMEDCHANGER_TARGET_TOOL - 1u), OFF);

}

TEST(Processing, ToleratesUserSwitchChangeDuringToolMoving)
{
  /* number change during moving, then another change during reversing, then another change during locking */

  /* start moving */
  helper_set_run_check(1u, 0u, FORWARD);

  /* change switch as we are in position to reverse */
  helper_set_run_check(2u, TOOL_REVERSE_POSITION(1u), FORWARD);
  helper_set_run_check(2u, TOOL_REVERSE_POSITION(2u), REVERSE);

  /* change switch as we are in position to lock */
  helper_set_run_check(3u, TOOL_LOCK_POSITION(2u), FORWARD);

  /* Allow it to complete (and check it does) */
  helper_set_run_check(3u, TOOL_REVERSE_POSITION(3u), REVERSE);
  helper_set_run_check(3u, TOOL_LOCK_POSITION(3u), LOCK);

}

TEST(Processing, ToleratesEncoderOffset)
{
  #define LOCKOFFSET_TARGET_TOOL_NUMBER  3u
  #define LOCKOFFSET_TOOL_POSITION_OFFSET_ERROR 1u

  helper_set_run_check(LOCKOFFSET_TARGET_TOOL_NUMBER, TOOL_LOCK_POSITION(LOCKOFFSET_TARGET_TOOL_NUMBER - 1u), FORWARD);
  helper_set_run_check(LOCKOFFSET_TARGET_TOOL_NUMBER, TOOL_REVERSE_POSITION(LOCKOFFSET_TARGET_TOOL_NUMBER), REVERSE);
  helper_set_run_check(LOCKOFFSET_TARGET_TOOL_NUMBER, TOOL_LOCK_POSITION(LOCKOFFSET_TARGET_TOOL_NUMBER) + LOCKOFFSET_TOOL_POSITION_OFFSET_ERROR, LOCK);
}


TEST(Processing, ToolNumberDecrement)
{
  #define TOOLDECR_TOOL_NUMBER_START 3u

	/* set tool number */
	set_tool_number_request(TOOLDECR_TOOL_NUMBER_START - 1u);

  for(uint8_t loop_idx = 0u; loop_idx < (NUM_TOOLS - 1u); loop_idx++)
  {
    /* set encoder position tool */
    {
        uint8_t current_tool_position = TOOLDECR_TOOL_NUMBER_START + loop_idx;
        if(current_tool_position > NUM_TOOLS)
        {
          current_tool_position -= NUM_TOOLS;
        }
   
        helper_set_run_check(TOOLDECR_TOOL_NUMBER_START - 1u, TOOL_LOCK_POSITION(current_tool_position), FORWARD);
    }
  }
  helper_set_run_check(TOOLDECR_TOOL_NUMBER_START - 1u, TOOL_REVERSE_POSITION(TOOLDECR_TOOL_NUMBER_START - 1u), REVERSE);
  helper_set_run_check(TOOLDECR_TOOL_NUMBER_START - 1u, TOOL_LOCK_POSITION(TOOLDECR_TOOL_NUMBER_START - 1u), LOCK);
}

TEST(Processing, ToolNumberIncremenButPositionMissed)
{
  #define MISSEDINCR_TARGET_TOOL_NUMBER  3u
  helper_set_run_check(MISSEDINCR_TARGET_TOOL_NUMBER, TOOL_LOCK_POSITION(MISSEDINCR_TARGET_TOOL_NUMBER - 1u), FORWARD);
  helper_set_run_check(MISSEDINCR_TARGET_TOOL_NUMBER, TOOL_LOCK_POSITION(MISSEDINCR_TARGET_TOOL_NUMBER + 1u), FORWARD);
  helper_set_run_check(MISSEDINCR_TARGET_TOOL_NUMBER, TOOL_REVERSE_POSITION(MISSEDINCR_TARGET_TOOL_NUMBER), REVERSE);
}

TEST(Processing, StartsUpInLock)
{
	processing_motor_request_t motor_request = get_motor_drive_request();
	CHECK_EQUAL(LOCK, motor_request);
}

TEST(Processing, SelectionOfEachTool)
{

  helper_set_run_check(0u, 0u, LOCK);

  for(uint8_t tool_idx = 1u; tool_idx < NUM_TOOLS; tool_idx++)
  {
    helper_set_run_check(tool_idx, TOOL_LOCK_POSITION(tool_idx - 1u), FORWARD);
    helper_set_run_check(tool_idx, TOOL_REVERSE_POSITION(tool_idx), REVERSE);
    helper_set_run_check(tool_idx, TOOL_LOCK_POSITION(tool_idx), LOCK);
  }

  for(uint8_t tool_idx = 2u; tool_idx < NUM_TOOLS; tool_idx++)
  {
    helper_set_run_check(NUM_TOOLS - tool_idx, TOOL_LOCK_POSITION(NUM_TOOLS - (tool_idx - 1u)), FORWARD);
    helper_set_run_check(NUM_TOOLS - tool_idx, TOOL_REVERSE_POSITION(NUM_TOOLS - tool_idx), REVERSE);
    helper_set_run_check(NUM_TOOLS - tool_idx, TOOL_LOCK_POSITION(NUM_TOOLS - tool_idx), LOCK);
  }

  helper_set_run_check(0u, TOOL_LOCK_POSITION(1u), FORWARD);
  helper_set_run_check(0u, TOOL_REVERSE_POSITION(0u), REVERSE);
  helper_set_run_check(0u, TOOL_LOCK_POSITION(0u), LOCK);

}

TEST(Processing, EncodertoToolMapping)
{
#define ENCODERMAPPING_DELIBERATE_OFFSET ((TOOL_LOCK_POSITION(1) >> 1u))
  /* Force no tool home position to be zero to force a a wrap within a tool range */
  for(uint8_t tool_idx = 0u; tool_idx < NUM_TOOLS; tool_idx++)
  {
    set_tool_encoder_position(tool_idx , TOOL_LOCK_POSITION(tool_idx) + ENCODERMAPPING_DELIBERATE_OFFSET);
  }

  for(uint8_t tool_idx = 0u; tool_idx < NUM_TOOLS; tool_idx++)
  {
    /* test all of the points in between this and the next */
    for(uint8_t offset_idx = 0u; offset_idx < TOOL_ENCODER_INTERVAL; offset_idx++)
    {
      uint8_t offset_amount;
      uint8_t encoder_position = (TOOL_LOCK_POSITION(tool_idx) + ENCODERMAPPING_DELIBERATE_OFFSET + offset_idx);
      if(encoder_position > ENCODER_TOP_VALUE)
      {
        encoder_position -= ENCODER_TOP_VALUE;
      }

      CHECK_EQUAL(tool_idx, convert_encoder_position_to_tool_range(encoder_position, &offset_amount));
      CHECK_EQUAL(offset_idx, offset_amount);
    }
  }
}



