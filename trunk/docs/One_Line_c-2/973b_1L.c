//
// file      : 973b_1L.c
// author    : Ian Forse
// date      : 16-FEB-2003
// purpose   : Demonstration of 5x7 character generation
// includes  : pic.h, delay.h, delay.c, 973b_dr.c, 973b_dr.h, 973b_rom_f.c
// notes     : 
// compiler  : Hi tech Compiler V8.00PL3
//
// Copyright (C) 2003 Ian Forse
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA

#include <pic.h>
__CONFIG (3F72h);
#define on 0xff
#define off 0x00
#define switch RB0
#include "973b_dr.h"
#include "delay.h"
#include "delay.c"
#include "973b_dr.c"
#include "973b_rom_f.c"


__EEPROM_DATA(0x8F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);


main()
{
	unsigned char a, b, c, i, temp1, z;
	z=1;
	dmx_start_up();
	TRISB0 = 1;
	RBPU = 0;

	dmx_clear_page (0);
	dmx_clear_page (1);
	dmx_clear_page (2);
	dmx_clear_page (3);
	
// This routine sets the electronic contast control.  The message below is displayed for
// 15s.  The INT button on the MDM-DEV-1 (RB0) should be pulled low.
	
	temp1 = EEPROM_READ(0x00);
	for (i=0; i<60; i++)
	{
		if ( !switch )
		{
			DelayMs(50); // debounce delay
			if ( !switch )
			{
				temp1 = temp1 + 1;
				if (temp1 > 0x9F) temp1 = 0x80;
				dmx973b_write_command (temp1 );
			}
		}

		dmx973b_start_posn (30, 0);
		c = dmx973b_char_1 ('D', off);
		c = dmx973b_char_1 ('M', off);
		c = dmx973b_char_1 ('X', off);
		c = dmx973b_char_1 ('9', off);
		c = dmx973b_char_1 ('7', off);
		c = dmx973b_char_1 ('3', off);
		c = dmx973b_char_1 ('B', off);
		
		dmx973b_start_posn (33, 1);
		c = dmx973b_char_1 ('L', off);
		c = dmx973b_char_1 ('a', off);
		c = dmx973b_char_1 ('s', off);
		c = dmx973b_char_1 ('c', off);
		c = dmx973b_char_1 ('a', off);
		c = dmx973b_char_1 ('r', off);
		
		dmx973b_start_posn (18, 2);
		c = dmx973b_char_1 ('E', off);
		c = dmx973b_char_1 ('l', off);
		c = dmx973b_char_1 ('e', off);
		c = dmx973b_char_1 ('c', off);
		c = dmx973b_char_1 ('t', off);
		c = dmx973b_char_1 ('r', off);
		c = dmx973b_char_1 ('o', off);
		c = dmx973b_char_1 ('n', off);
		c = dmx973b_char_1 ('i', off);
		c = dmx973b_char_1 ('c', off);
		c = dmx973b_char_1 ('s', off);
		
		dmx973b_start_posn (15, 3);
		c = dmx973b_char_1 ('B', off);
		c = dmx973b_char_1 ('y', off);
		c = dmx973b_char_1 (' ', off);
		c = dmx973b_char_1 ('I', off);
		c = dmx973b_char_1 ('a', off);
		c = dmx973b_char_1 ('n', off);
		c = dmx973b_char_1 (' ', off);
		c = dmx973b_char_1 ('F', off);
		c = dmx973b_char_1 ('o', off);
		c = dmx973b_char_1 ('r', off);
		c = dmx973b_char_1 ('s', off);
		c = dmx973b_char_1 ('e', off);
		
		DelayMs(250);
	}
	eeprom_write(0x00, temp1);
	
	while(z)
	{
		dmx_clear_page (0);
		dmx_clear_page (1);
		dmx_clear_page (2);
		dmx_clear_page (3);
		
		dmx973b_start_posn (0, 0);
		a = 0;
		b = 0;
		for (i=0x20; i<0x7F; i++)
		{
			if (a > 94)
			{
				a = 0;
				b = b + 1;
				if (b > 3)
				{
					b = 3;
					dmx973b_page_copy (1, 0);
		  			dmx973b_page_copy (2, 1);
		  			dmx973b_page_copy (3, 2);
		  			dmx_clear_page (3);
		  		}
		  		dmx973b_start_posn (a, b);
		  	}
		  	c = dmx973b_char_1 (i, off);
		  	DelayMs(250);
		  	a = a + c;
		}
	}
}
