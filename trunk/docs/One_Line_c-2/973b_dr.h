//
// file      : 973b_dr.h
// author    : Ian Forse
// date      : 09-FEB-2003
// purpose   : DMX973B low level drivers
// includes  : None
// notes     : 
// compiler  : Hi tech Compiler V8.00PL3
//
// Copyright (C) 2003 Ian Forse
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA


extern unsigned char dmx973b_read_command (void);

extern unsigned char dmx973b_read_data (void);

extern void dmx973b_write_command (unsigned char data1);

extern void dmx973b_write_data (unsigned char data2);

extern unsigned char dmx973b_read_pos (unsigned char dmx_page1, unsigned char dmx_col1);

extern void dmx973b_write_pos (unsigned char dmx_page2, unsigned char dmx_col2, unsigned char dmx_data4);

extern void dmx973b_write_next ( unsigned char dmx_data5);

extern void dmx_clear_page (unsigned char page_to_clear);

extern void dmx_start_up (void);

extern void dmx973b_start_posn (unsigned char start_col, unsigned char start_page);

extern void dmx973b_page_copy (unsigned char page_from, unsigned char page_to);




