#include "types.h"
#include "board.h"
#include "processing.h"
#include "toolchange_fsm.h"
#include "toolchange_modecon.h"

//HACK - use accessors!
extern tool_changer_status_t tool_changer_status;

void init_processing(void)
{
  modecon_init();
  init_toolchange_fsm();
  
  for(uint8_t tool_idx = 0u; tool_idx < NUM_TOOLS; tool_idx++)
  { 
    set_tool_encoder_position(tool_idx, toolchanger_tool_positions[tool_idx]);
  }
  return;
}

void run_processing(void)
{
  /* Allow the mode controller to select which tool we want */
  {
    modecon_set_automode(tool_changer_status.auto_switch_state);
    modecon_set_usertoolrequest(tool_changer_status.user_tool_selection);
  
    modecon_run_toolselection();

    tool_changer_status.toolchanger_mode = modecon_get_mode();
  }

  /* Allow the Toolchanger FSM to select how the motor would be driven */
  {
    set_tool_number_request(modecon_get_tool_request());
    set_encoder_position(tool_changer_status.encoder_position);
    
    run_toolchanger_fsm();

    tool_changer_status.current_tool_number = modecon_get_tool_request();
    tool_changer_status.changer_state = changerfsm_get_state();
  }

  /* Allow the mode controller to select how the motor should be driven */
  {
    processing_motor_request_t motor_request = get_motor_drive_request();
    modecon_set_normalmotorcontrol(motor_request);
    
    modecon_run_motorselection();
    tool_changer_status.motor_command = modecon_get_motorcontrol();
  }

  return;
}


