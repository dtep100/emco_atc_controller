//
// file      : dmx973b.h
// author    : Ian Forse
// date      : 26-NOV-2002
// purpose   : ADC setup, reading and format conversion header file
// includes  : None
// notes     : 
// compiler  : Hi tech Compiler V8.00PL3
//
// Copyright (C) 2002 Ian Forse
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA


extern unsigned char dmx973b_read_command (void);

extern unsigned char dmx973b_read_data (void);

extern void dmx973b_write_command (unsigned char data1);

extern void dmx973b_write_data (unsigned char data2);

extern unsigned int dmx973b_read_pos (unsigned int dmx_page1, unsigned int dmx_col1);

extern void dmx973b_write_pos (unsigned char dmx_page2, unsigned char dmx_col2, unsigned char dmx_data4);

extern void dmx973b_write_next ( unsigned char dmx_data5);

extern void dmx_clear_disp (void);
