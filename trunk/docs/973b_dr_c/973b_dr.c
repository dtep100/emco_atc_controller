//
// file      : 973b_dr.c
// author    : Ian Forse
// date      : 09-FEB-2003
// purpose   : DMX973B low level drivers
// includes  : 973b_dr.h
// notes     : 
// compiler  : Hi tech Compiler V8.00PL3
//
// Copyright (C) 2003 Ian Forse
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA


#include "973b_dr.h"

// Epson SED1530 Constants from datasheet
#define dmx973b_reset 0xE2
#define dmx973b_bias 0xA3
#define dmx973b_adc 0xA0
#define dmx973b_power 0x2F
#define dmx973b_Disp_On 0xAF
#define dmx973b_Page_base 0xB0
#define dmx973_Top_base 0xC0
#define dmx973b_Col_base_l 0x00
#define dmx973b_Col_base_h 0x11

// Port definitions.  This almost matches the MDM-DEV-1 board DMX Display
// interface on PL2.  Contrast Pot removed and now driven by RE2.
// Change for appropriate PIC.

#define DMX973_D PORTD
#define DMX973_RS RB3
#define DMX973_RW RB6
#define DMX973_E RB7
#define DMX973_Reset RE2

#define DMX973_D_direction TRISD
#define DMX973_RS_direction TRISB3
#define DMX973_RW_direction TRISB6
#define DMX973_E_direction TRISB7
#define DMX973_Reset_direction TRISE2

#define high (1)
#define low (0)
#define input (1)
#define output (0)
#define all_output 0x00
#define all_input 0xff


// Read Status
unsigned char dmx973b_read_command (void)
{
	unsigned char dmx_temp1;
  	DMX973_D = 0xFF;
  	DMX973_RW = high;
  	DMX973_RS = low;
  	DMX973_D_direction = all_input;
  	DMX973_E  = high;
  	dmx_temp1 = DMX973_D;
  	DMX973_E  = low;
  	DMX973_D_direction = all_output;
  	return dmx_temp1;
}


// Read Display Data
unsigned char dmx973b_read_data (void)
{
	unsigned char dmx_temp2;
  	DMX973_D = 0xFF;
  	DMX973_RW = high;
  	DMX973_RS = high;
  	DMX973_D_direction = all_input;
  	DMX973_E  = high;
  	dmx_temp2 = DMX973_D;
  	DMX973_E  = low;
  	DMX973_D_direction = all_output;
  	return dmx_temp2;
}


// Write commands i.e. page and column addresses
void dmx973b_write_command (unsigned char data1 )
{
	DMX973_RW = low;
  	DMX973_RS = low;
  	DMX973_D = data1;
  	DMX973_E  = high;
  	DMX973_E  = low;
}


// Write display data
void dmx973b_write_data (unsigned char data2 )
{
	DMX973_RW = low;
  	DMX973_RS = high;
  	DMX973_D = data2;
  	DMX973_E  = high;
  	DMX973_E  = low;
}


// Read display data from a position. Parameters are page and column with data being returned
// The first read is a dummy and is as per the datasheet.
unsigned char dmx973b_read_pos ( unsigned char dmx_page1, unsigned char dmx_col1 )
{
	unsigned char dmx_data3;
	dmx973b_write_command (dmx973b_Page_base + ( dmx_page1 ));
  	dmx973b_write_command (dmx973b_Col_base_h + ( ( dmx_col1 & 0xF0 ) >> 4 ));
  	dmx973b_write_command (dmx973b_Col_base_l + ( dmx_col1 & 0x0F ));
  	dmx_data3 = dmx973b_read_data ();
  	dmx_data3 = dmx973b_read_data();
  	return dmx_data3;
}


// Write display data to a position. Parameters are page, column and data.
void dmx973b_write_pos (unsigned char dmx_page2, unsigned char dmx_col2, unsigned char dmx_data4 )
{
	dmx973b_write_command (dmx973b_Page_base + ( dmx_page2 ));
  	dmx973b_write_command (dmx973b_Col_base_h + ( ( dmx_col2 & 0xF0 ) >> 4 ));
  	dmx973b_write_command (dmx973b_Col_base_l + ( dmx_col2 & 0x0F ));
  	dmx973b_write_data (dmx_data4);
}


// Write to the next location.  This is only used after a write to a known location
void dmx973b_write_next ( unsigned char dmx_data5 )
{
	dmx973b_write_data (dmx_data5);
}


// Clear one page of display (pages 0 to 3)
void dmx_clear_page (unsigned char page_to_clear)
{
	unsigned char i;
	dmx973b_write_pos ( page_to_clear , 0x00 , 0x00 );
	for (i=0; i<100; i++)
	{
  		dmx973b_write_next ( 0x00 );
  	}
}


// Initialisation of the SED1530
void dmx_start_up (void)
{
	ADCON1 = 0x84; 		// Set port E0/1/2 all digital - leave ADC
	DMX973_D = 0x00;	// Port Initialisation
	DMX973_RS = low;
	DMX973_RW = low;
	DMX973_E = low;
	DMX973_Reset = high;
	
	DMX973_Reset_direction = output;
	DMX973_D_direction = all_output;
	DMX973_RS_direction = output;
	DMX973_RW_direction = output;
	DMX973_E_direction = output;

	DMX973_Reset = low;	// Minimum reset @ 5V = 0.5us
	#asm
	nop
	#endasm
	DMX973_Reset = high;
	DMX973_E = low;
	
	dmx973b_write_command ( dmx973b_reset );
	dmx973b_write_command ( dmx973b_bias );		// change Bias to 1/5
	dmx973b_write_command ( dmx973b_adc );		// ADC right is a0
	dmx973b_write_command ( EEPROM_READ(0x00) );	// Contrast Control
	dmx973b_write_command ( dmx973b_power );	// turn power boost circuit on
	dmx973b_write_command ( dmx973b_Disp_On );
	dmx973b_write_command ( dmx973b_Page_base );	// Display page = 0
	dmx973b_write_command ( dmx973b_Col_base_h );	// Set high nibble column = 0
	dmx973b_write_command ( dmx973b_Col_base_l );	// Set low nibble column = 0
	dmx973b_write_command ( 0x40 );			// Set start line 40   for 973=c0  MSC modules=d0
}


// Set the start position.
void dmx973b_start_posn (unsigned char start_col, unsigned char start_page)
{
	dmx973b_write_command (dmx973b_Page_base + ( start_page ));
  	dmx973b_write_command (dmx973b_Col_base_h + ( ( start_col & 0xF0 ) >> 4 ));
  	dmx973b_write_command (dmx973b_Col_base_l + ( start_col & 0x0F ));
}


// Copy from one page to another, useful in terminal mode where each line is shifted
void dmx973b_page_copy (unsigned char page_from, unsigned char page_to)
{
	unsigned char i, dmx_data6;
	for (i=0; i<100; i++)
	{
		dmx_data6 = dmx973b_read_pos ( page_from, i );
  		dmx973b_write_pos (page_to, i, dmx_data6 );
  	}
}
