#include "types.h"
#include "board.h"

/**********************************************************************/
/** @brief Decode the encoder gray code output into binary, using the 
 *           form MSB = MSB, others = Bit N XOR Bit N+1.
 *                                                    
 *  @param encoder_word The encoder output.   
 *  @return Binary equivalent.                                       
/**********************************************************************/
uint8_t convert_encoder_format_to_binary_position(uint8_t encoder_word)
{
  uint8_t retval = 0u;
  uint8_t mask;

  mask = 1u << (NUM_ENCODER_BITS - 1u);
  for(uint8_t bit_idx = 0u; bit_idx < NUM_ENCODER_BITS; bit_idx++)
  {
    if(bit_idx)
    {
      /* Output bit N = Input bit N XOR bit N+1 */
      retval |= (encoder_word & mask) ^ ((retval & (mask << 1u)) >> 1u);
    }
    else
    {
      /* Direct copy */
      retval |= encoder_word & mask;
    }
    mask >>= 1u;
  }

  return(retval);
}
