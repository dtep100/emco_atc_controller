#include "types.h"
#include "board.h"
#include "toolchange_modecon.h"

static uint8_t automatic_mode_tool_request;
static uint8_t manual_mode_tool_request;

static uint8_t tool_command;
static processing_motor_request_t motor_command;

static toolchanger_mode_t tool_changer_mode;
static bool_t test_mode_enabled;
static bool_t auto_mode_enabled;

static processing_motor_request_t test_motor_request;
static processing_motor_request_t normal_motor_request;

static void modecon_update_mode(void);

void modecon_init(void)
{
  tool_command = 0u;
  tool_changer_mode = MANUAL;
  test_mode_enabled = false;
  auto_mode_enabled = false;
}

void modecon_set_automode(bool_t automode)
{
  auto_mode_enabled = automode;
}

void modecon_set_hosttoolrequest(uint8_t tool_number)
{
  /* The rest of the changer counts 0 as the first tool */
  automatic_mode_tool_request = tool_number - 1u;
}

void modecon_set_usertoolrequest(uint8_t tool_number)
{
  manual_mode_tool_request = tool_number;
}

void modecon_run_toolselection(void)
{
  modecon_update_mode();

  if(AUTOMATIC == tool_changer_mode)
  {
    tool_command = automatic_mode_tool_request;
  }
  else if(MANUAL == tool_changer_mode)
  {
    tool_command = manual_mode_tool_request;
  }
  else
  {
  }
}


uint8_t modecon_get_tool_request(void)
{
  return(tool_command);
}

toolchanger_mode_t modecon_get_mode(void)
{
  return(tool_changer_mode);
}

void modecon_run_motorselection(void)
{
  modecon_update_mode();

  if(TEST == tool_changer_mode)
  {
    motor_command = test_motor_request;
  }
  else
  {
    motor_command = normal_motor_request;
  }

  return;
}

static void modecon_update_mode(void)
{
  if(test_mode_enabled)
  {
    tool_changer_mode = TEST;
  }
  else if(auto_mode_enabled)
  {
    tool_changer_mode = AUTOMATIC;
  }
  else
  {
    tool_changer_mode = MANUAL;
  }
}

void modecon_set_testmode(bool_t enable)
{
  test_mode_enabled = enable;
}

void modecon_set_testmotorcontrol(processing_motor_request_t motor_request)
{
  test_motor_request = motor_request;
}

void modecon_set_normalmotorcontrol(processing_motor_request_t motor_request)
{
  normal_motor_request = motor_request;
}

processing_motor_request_t modecon_get_motorcontrol(void)
{
  return(motor_command);
}
