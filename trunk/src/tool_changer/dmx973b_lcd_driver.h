void lcd_set_cursor(uint8_t line_idx);
void lcd_print_ascii_char(char character);
void lcd_print_dec_char(uint8_t number_val, uint8_t num_chars);
void lcd_print_hex_char(uint8_t number_val, uint8_t num_chars);
void init_lcd(void);
