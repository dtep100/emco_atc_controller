#ifndef TEST_HARNESS
#include <Arduino.h>
#include "arduino_serial_driver.h"

#else

/* Mocks for test environment */
#define INPUT               (0u)
#define INPUT_PULLUP        (1u)
#define OUTPUT              (2u)
#define digitalRead(x)      (0u)
#define digitalWrite(x, y)  (0u)
#define pinMode(x, y)       (0u)

#endif


/************************* Drivers ****************************/
#include "E6CP-AG5C_encoder_driver.h"
#include "dmx973b_lcd_driver.h"

/**************************************************************/



/************************** LCD *******************************/
/* LCD Pin listing */
#define   LCD_A0   53
#define   LCD_E    51
#define   LCD_RNW  50
#define   LCD_NRES 52

#define LCD_DATA_PORT_WIDTH 8u
const uint8_t pin_list[LCD_DATA_PORT_WIDTH] = 
{
  48, /* LCD_D0 */
  49, /* LCD_D1 */
  46, /* LCD_D2 */
  47, /* LCD_D3 */
  44, /* LCD_D4 */
  45, /* LCD_D5 */
  42, /* LCD_D6 */
  43  /* LCD_D7 */
 };

/*************************************************************/



/*********************** USER INPUTS *************************/
/* User switch pin listing */
#define NUM_ROTARY_POSITIONS  5u
#define MANAUTO_SEL 13

const uint8_t man_tool_input_pin_list[NUM_ROTARY_POSITIONS] = 
{
  26, /* Position 1 */
  27, /* Position 2 */
  28, /* Position 3 */
  29, /* Position 4 */
  30 /* Position 5 */
};

const uint8_t encoder_input_pin_list[NUM_ENCODER_BITS] = 
{
  25, /* Bit 0 */
  24, /* Bit 1 */
  23, /* Bit 2 */
  22, /* Bit 3 */
  18, /* Bit 4 */
  21, /* Bit 5 */
  20, /* Bit 6 */
  19  /* Bit 7 */
};

/*************************************************************/


/******************** Tool changer setup *********************/
#define NUM_TOOLS 6u
#define ENCODER_RANGE (1u << NUM_ENCODER_BITS)
#define ENCODER_TOP_VALUE (ENCODER_RANGE - 1u)
#define ENCODER_RANGE_MASK (ENCODER_RANGE - 1u)
#define ENCODER_TOOL_INTERVAL (ENCODER_RANGE/NUM_TOOLS)


const uint8_t toolchanger_tool_positions[NUM_TOOLS] = 
{
  104u,
  150u,
  192u,
  234u,
  20u,
  62u
};

/*************************************************************/


/***************** MOTOR CONTROL OUTPUTS *********************/
/* Motor relay pin listing */
#define MOTOR_POS_RELAY  6
#define MOTOR_NEG_RELAY  4
#define MOTOR_SPEED_RELAY  7

/*************************************************************/
