#include "types.h"
#include "board.h"
#include "toolchange_modecon.h"

#include "ModbusRtu.h"

//HACK - use accessors!
extern tool_changer_status_t tool_changer_status;

#define MAJOR_VERSION 1u
#define MINOR_VERSION 0u

#if 0 /* Hyperteminal comms mode */
void init_host_control(void)
{
  Serial.begin(19200);
  Serial.println("Initialised EMCO ATC Controller");
}

void read_host_control_inputs(void)
{
  uint8_t rx_bytes = Serial.available();
  if(rx_bytes)
  {
    uint8_t rx_cmd = Serial.read();
  
    switch(rx_cmd)
    {
      case 'S': /* Get Status */
      {
        Serial.print("ACK:");
        Serial.print(" State:");
        Serial.print(tool_changer_status.changer_state);
        Serial.print(" Tool:");
        Serial.print(tool_changer_status.current_tool_number);
        Serial.print(" Encoder:");
        Serial.print(tool_changer_status.encoder_position);
        Serial.print(" Motor:");
        Serial.print(tool_changer_status.motor_command);
        Serial.print(" Auto/Man SW:");
        Serial.print(tool_changer_status.toolchanger_mode);
        Serial.print(" User Tool Sel:");
        Serial.println(tool_changer_status.user_tool_selection);
      }break;
      
      case 'T': /* Set test mode*/
      {
        modecon_set_testmode(true);
        Serial.println("ACK, Entering test mode");
      }break;
      
      case 'N': /* Set normal mode*/
      {
        modecon_set_testmode(false);
        Serial.println("ACK, Leaving test mode");
      }break;

      case 'F': /* Test mode command - Forward Motor */
      {
        modecon_set_testmotorcontrol(FORWARD);
        Serial.println("ACK, Setting test control to FORWARD");
      }break;
      
      case 'R': /* Test mode command - Reverse Motor */
      {
        modecon_set_testmotorcontrol(REVERSE);
        Serial.println("ACK, Setting test control to REVERSE");
      }break;

      case 'L': /* Test mode command - Lock Motor */
      {
        modecon_set_testmotorcontrol(LOCK);
        Serial.println("ACK, Setting test control to LOCK");
      }break;

      case 'O': /* Test mode command - Off Motor */
      {
        modecon_set_testmotorcontrol(OFF);
        Serial.println("ACK, Setting test control to OFF");
      }break;

      case 'V': /* Version info */
      {
        Serial.print("ACK, Version: V");
        Serial.print(MAJOR_VERSION);
        Serial.print(".");
        Serial.println(MINOR_VERSION);
      }break;
      
      default: /* Includes tool set commands (1..NUM_TOOLS) */
      {
        uint8_t tool_request = rx_cmd - '0';
        if((tool_request > 0) && (tool_request <= NUM_TOOLS))
        {    
          modecon_set_hosttoolrequest(tool_request);
          tool_changer_status.host_tool_selection = tool_request;
          Serial.print("ACK, Setting tool#: ");
          Serial.println(tool_request);
        }
        else
        {
          Serial.print("NACK:");
          Serial.println(rx_cmd);
        }
      }break;
    }

    /* Store for debug */
    tool_changer_status.last_cmd_recieved = rx_cmd;

    /* Purge the buffer */
    Serial.flush();
  }
  return;
}

#else /* Modbus comms mode */

/* data array for modbus network sharing */
#define MEMMAP_SIZE 9u
uint16_t modbus_memorymap[MEMMAP_SIZE];

#define MEMMAP_IDX_STATE        0
#define MEMMAP_IDX_COMMAND      1
#define MEMMAP_IDX_CURRTOOL     2
#define MEMMAP_IDX_HSTREQTOOL   3
#define MEMMAP_IDX_USRREQTOOL   4
#define MEMMAP_IDX_ENCPOSITION  5
#define MEMMAP_IDX_MOTORCOMMAND 6
#define MEMMAP_IDX_CHANGERMODE  7
#define MEMMAP_IDX_VERSIONINFO  8

#define COMMAND_NOCMD         0x0000u
#define COMMAND_TOOLNUMBASE   0u
#define COMMAND_ENTERTESTMODE 10u
#define COMMAND_TESTFORWARD   11u
#define COMMAND_TESTREVERSE   12u
#define COMMAND_TESTLOCK      13u
#define COMMAND_TESTOFF       14u
#define COMMAND_EXITTESTMODE  15u

/**
 *  Modbus object declaration
 *  u8id : node id = 0 for master, = 1..247 for slave
 *  u8serno : serial port (use 0 for Serial)
 *  u8txenpin : 0 for RS-232 and USB-FTDI 
 *               or any pin number > 1 for RS-485
 */
Modbus slave(1,0,0); // this is slave @1 and RS-232 or USB-FTDI

void init_host_control(void)
{
  slave.begin( 19200, SERIAL_8E1 ); // 19200 baud, 8-bits, even, 1-bit stop
}

void read_host_control_inputs(void)
{
  /* Refresh the memory map */
  modbus_memorymap[MEMMAP_IDX_COMMAND]      = COMMAND_NOCMD;
  modbus_memorymap[MEMMAP_IDX_STATE]        = tool_changer_status.changer_state;
  modbus_memorymap[MEMMAP_IDX_CURRTOOL]     = tool_changer_status.current_tool_number;
  modbus_memorymap[MEMMAP_IDX_HSTREQTOOL]   = tool_changer_status.host_tool_selection;
  modbus_memorymap[MEMMAP_IDX_USRREQTOOL]   = tool_changer_status.user_tool_selection;
  modbus_memorymap[MEMMAP_IDX_ENCPOSITION]  = tool_changer_status.encoder_position;
  modbus_memorymap[MEMMAP_IDX_MOTORCOMMAND] = tool_changer_status.motor_command;
  modbus_memorymap[MEMMAP_IDX_CHANGERMODE]  = tool_changer_status.toolchanger_mode;
  modbus_memorymap[MEMMAP_IDX_VERSIONINFO]  = (MAJOR_VERSION << 4u | MINOR_VERSION);

  /* Check for command */
  slave.poll( modbus_memorymap, MEMMAP_SIZE );
  
  /* Action any new commands */
  uint8_t command = modbus_memorymap[MEMMAP_IDX_COMMAND];
  if(command != COMMAND_NOCMD)
  {
    tool_changer_status.last_cmd_recieved = (uint8_t)command;
    switch(command)
    {
      case COMMAND_ENTERTESTMODE: /* Set test mode*/
      {
        modecon_set_testmode(true);
      }break;
      
      case COMMAND_EXITTESTMODE: /* Set normal mode*/
      {
        modecon_set_testmode(false);
      }break;

      case COMMAND_TESTFORWARD: /* Test mode command - Forward Motor */
      {
        modecon_set_testmotorcontrol(FORWARD);
      }break;
      
      case COMMAND_TESTREVERSE: /* Test mode command - Reverse Motor */
      {
        modecon_set_testmotorcontrol(REVERSE);
      }break;

      case COMMAND_TESTLOCK: /* Test mode command - Lock Motor */
      {
        modecon_set_testmotorcontrol(LOCK);
      }break;

      case COMMAND_TESTOFF: /* Test mode command - Off Motor */
      {
        modecon_set_testmotorcontrol(OFF);
      }break;
      
      default: /* Includes tool set commands (1..NUM_TOOLS) */
      {
        uint8_t tool_request = command - COMMAND_TOOLNUMBASE;
        if((tool_request > 0) && (tool_request <= NUM_TOOLS))
        {    
          modecon_set_hosttoolrequest(tool_request);
          tool_changer_status.host_tool_selection = tool_request;
        }
      }break;
    }
  }
}

#endif
