void init_input(void);
uint8_t read_encoder_state(void);
void read_user_switch_inputs(bool_t* auto_switch_ptr, uint8_t* manual_sel_ptr);
void read_pc_control_inputs(void);
