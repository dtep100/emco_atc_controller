#include "types.h"
#include "board.h"
#include "input.h"


void init_input(void)
{
  /* user inputs */
  pinMode (MANAUTO_SEL, INPUT_PULLUP);
  for(uint8_t switch_input_idx = 0u; switch_input_idx < NUM_ROTARY_POSITIONS; switch_input_idx++)
  {
    pinMode (man_tool_input_pin_list[switch_input_idx], INPUT_PULLUP);
  }

  /* encoder inputs */
  for(uint8_t encoder_input_idx = 0u; encoder_input_idx < NUM_ENCODER_BITS; encoder_input_idx++)
  {
    pinMode (encoder_input_pin_list[encoder_input_idx], INPUT_PULLUP);
  }

  return;
}


uint8_t read_encoder_state(void)
{
  uint8_t encoder_position;
  uint8_t encoder_word = 0;
  
  /* Read the value from the pins */
  for(uint8_t encoder_input_idx = 0u; encoder_input_idx < NUM_ENCODER_BITS; encoder_input_idx++)
  {
    if(!(digitalRead(encoder_input_pin_list[encoder_input_idx])))
    {
      encoder_word |= 1u << encoder_input_idx;
    }
  }

  /* convert the encoder output format to a binary position value */
  encoder_position = convert_encoder_format_to_binary_position(encoder_word);
  
  //apply direction conversion here if needed

  return(encoder_position);
}

/**********************************************************************/
/** @brief Reads the user input selections
 *                                                                                       
/**********************************************************************/
void read_user_switch_inputs(bool_t* auto_switch_ptr, uint8_t* manual_sel_ptr)
{
  bool_t auto_operation_requested;
  uint8_t manual_tool_requested = 0u;
  
  /* Read manual/auto switch */
  auto_operation_requested = false;
  if(!(digitalRead(MANAUTO_SEL)))
  { /* Automatic operation must be requested by making a switch conn, default to manual*/
    auto_operation_requested = true;
  }
  

  /* Read tool selection rotary switch */
  /* one-hot encoded - inverse logic, no closures = tool zero */
  manual_tool_requested = 0u;
  for(uint8_t switch_input_idx = 0u; switch_input_idx < NUM_ROTARY_POSITIONS; switch_input_idx++)
  {
    if(!(digitalRead(man_tool_input_pin_list[switch_input_idx])))
    {
      manual_tool_requested = switch_input_idx + 1u;
      break;
    }
  }

  /* Return results */
  *auto_switch_ptr = auto_operation_requested;
  *manual_sel_ptr = manual_tool_requested;
  return;
}

