
typedef unsigned char uint8_t;
typedef uint8_t bool_t;

enum processing_motor_request_t
{
  OFF,
  FORWARD,
  REVERSE,
  LOCK
};

enum toolchange_fsm_state_t
{
  IDLE,
  MOVING_FORWARD,
  MOVING_BACKWARDS,
  LOCKING,
  ERROR
};

enum toolchanger_mode_t
{
  MANUAL,
  AUTOMATIC,
  TEST
};


typedef struct tool_changer_status_tag
{
  bool_t auto_switch_state;
  toolchanger_mode_t toolchanger_mode;
  uint8_t current_tool_number;
  toolchange_fsm_state_t changer_state;
  uint8_t last_cmd_recieved;
  uint8_t host_tool_selection;
  uint8_t user_tool_selection;
  uint8_t encoder_position;
  processing_motor_request_t motor_command;
  
}tool_changer_status_t;

