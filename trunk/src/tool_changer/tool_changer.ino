#include "types.h"
#include "board.h"
#include "processing.h"
#include "input.h"
#include "output.h"

tool_changer_status_t tool_changer_status;

void setup()
{
  init_input();
  init_output();
  init_processing();
  init_host_control();
}

void loop() 
{  
  /* read inputs */
  tool_changer_status.encoder_position = read_encoder_state();
  read_user_switch_inputs(&tool_changer_status.auto_switch_state, &tool_changer_status.user_tool_selection);
  read_host_control_inputs();

  /* process */
  run_processing();

  /* update outputs */
  update_display(tool_changer_status.toolchanger_mode,
                 tool_changer_status.current_tool_number,
                 tool_changer_status.changer_state,
                 tool_changer_status.last_cmd_recieved,
                 tool_changer_status.host_tool_selection,
                 tool_changer_status.user_tool_selection,
                 tool_changer_status.encoder_position,
                 tool_changer_status.motor_command);
                 
  update_motor_controls(tool_changer_status.motor_command);

}
