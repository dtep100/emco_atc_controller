void init_output(void);
void update_pc_status(void);
void update_motor_controls(processing_motor_request_t command);
void update_display(toolchanger_mode_t toolchanger_mode,
                    uint8_t tool_number,
                    toolchange_fsm_state_t changer_state,
                    uint8_t last_host_cmd,
                    uint8_t host_tool_selection,
                    uint8_t user_tool_selection,
                    uint8_t encoder_value,
                    processing_motor_request_t motor_command);

