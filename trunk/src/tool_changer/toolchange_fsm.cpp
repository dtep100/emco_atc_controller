#include "types.h"
#include "board.h"
#include "toolchange_fsm.h"

static toolchange_fsm_state_t toolchange_fsm_state;
static uint8_t current_encoder_position;
static uint8_t tool_selection;
static uint8_t tool_encoder_position_list[NUM_TOOLS];
static processing_motor_request_t motor_request;


void init_toolchange_fsm(void)
{
  motor_request = LOCK;
  toolchange_fsm_state = IDLE;
  return;
}

void run_toolchanger_fsm(void)
{
  processing_motor_request_t next_motor_request = LOCK;
  toolchange_fsm_state_t next_state = toolchange_fsm_state;
  uint8_t current_tool_offset;

  switch (toolchange_fsm_state)
  {
    case MOVING_FORWARD:
    {
      if(test_if_encoder_in_reversing_range_for_tool_selection(tool_selection, current_encoder_position))
      {
        next_state = MOVING_BACKWARDS;
        next_motor_request = REVERSE;
      }
      else
      {
        next_state = MOVING_FORWARD;
        next_motor_request = FORWARD;
      }
    }break;

    case MOVING_BACKWARDS:
    {
      if(tool_selection == convert_encoder_position_to_tool_range(current_encoder_position, &current_tool_offset))
      {
        if(test_if_encoder_in_locking_range_for_tool_selection(tool_selection, current_encoder_position))
        {
          next_state = LOCKING;
          next_motor_request = LOCK;
        }
        else
        {
          next_state = MOVING_BACKWARDS;
          next_motor_request = REVERSE;
        }
      }
      else
      {
        next_state = MOVING_FORWARD;
        next_motor_request = FORWARD;
      }

    }break;

    case LOCKING:
    {
      if(tool_selection == convert_encoder_position_to_tool_range(current_encoder_position, &current_tool_offset))
      {
        next_state = IDLE;
        next_motor_request = LOCK;
      }
      else
      {
        next_state = MOVING_FORWARD;
        next_motor_request = FORWARD;
      }
    }break;

    default:
    case ERROR:
    case IDLE:
    {
      if(tool_selection != convert_encoder_position_to_tool_range(current_encoder_position, &current_tool_offset))
      {
        next_state = MOVING_FORWARD;
        next_motor_request = FORWARD;
      }
      else
      {
        if(toolchange_fsm_state == ERROR)
        {
          next_state = ERROR;
          next_motor_request = OFF;
        }
        else
        {
          next_state = IDLE;
          next_motor_request = LOCK;
        }
      }
    }break;
  }

  //fixme - would be good to derive the control output from the next state since they are linked - then there can be no mistakes.

  toolchange_fsm_state = next_state;
  motor_request = next_motor_request;
  return;
}

processing_motor_request_t get_motor_drive_request(void)
{
  return motor_request;
}

void set_tool_number_request(uint8_t requested_tool)
{
  tool_selection = requested_tool;
  return;
}

void set_encoder_position(uint8_t encoder_position)
{
  current_encoder_position = encoder_position;
  return;
}

void set_tool_encoder_position(uint8_t tool_number, uint8_t encoder_value)
{
  if(tool_number < NUM_TOOLS)
  {
    tool_encoder_position_list[tool_number] = encoder_value;
  }
  return;
}

uint8_t convert_encoder_position_to_tool_range(uint8_t encoder_position, uint8_t* position_within_range)
{
  uint8_t ret_val = 0u;

  /* Loop the tools */
  for(uint8_t tool_number = 0u; tool_number < NUM_TOOLS; tool_number++)
  {
    uint8_t range_first_position = tool_encoder_position_list[tool_number];
    uint8_t range_end_position;
    /* check if we are on the lst tool and need to wrap */
    if(tool_number == (NUM_TOOLS - 1u))
    {
      range_end_position = tool_encoder_position_list[0u];
    }
    else
    {
      range_end_position = tool_encoder_position_list[tool_number + 1u];
    }

    /* Apply a different test if this tool range crosses the encoder wrap point */
    if(range_end_position > range_first_position)
    {
      if((encoder_position >= range_first_position) && 
         (encoder_position < range_end_position))
      {
        ret_val = tool_number;
        *position_within_range = encoder_position - range_first_position;
        break;
      }
    }
    else
    {
      if(encoder_position >= range_first_position)
      {
        ret_val = tool_number;
        *position_within_range = encoder_position - range_first_position;
        break;
      }
      else if(encoder_position < range_end_position)
      {
        /* encoder has gone beyond 0 */
        ret_val = tool_number;
        *position_within_range = ((ENCODER_TOP_VALUE - range_first_position) + encoder_position + 1u);
        break;
      }
      else
      {
      }
    }
  }
  return(ret_val);
}


bool_t test_if_encoder_in_reversing_range_for_tool_selection(uint8_t tool_selection, uint8_t encoder_position)
{
  #define ENCODER_OVERSHOOT_MARGIN (ENCODER_TOOL_INTERVAL >> 2u)
  #define ENCODER_REVERSE_MARGIN (ENCODER_TOOL_INTERVAL >> 2u)
  uint8_t current_tool_offset;
  uint8_t current_tool_range = convert_encoder_position_to_tool_range(encoder_position, &current_tool_offset);
  bool_t ret_val = false;

  if(current_tool_range == tool_selection)
  {
    /* test the current position within the tool range */
    if((current_tool_offset < (ENCODER_TOOL_INTERVAL - ENCODER_OVERSHOOT_MARGIN)) && /* not too close the the end that we may overshoot before the relays close */
       (current_tool_offset > ENCODER_REVERSE_MARGIN)) /* moved far enough past the ratchet point */
    {
      ret_val = true;
    }
  }
  return(ret_val);
}

bool_t test_if_encoder_in_locking_range_for_tool_selection(uint8_t tool_selection, uint8_t encoder_position)
{
  uint8_t current_tool_offset;
  uint8_t current_tool_range = convert_encoder_position_to_tool_range(encoder_position, &current_tool_offset);
  bool_t ret_val = false;

  if(current_tool_range == tool_selection)
  {
    /* test the current position within the tool range */
    if(current_tool_offset < (ENCODER_TOOL_INTERVAL >> 3u))
    {
      ret_val = true;
    }
  }
  return(ret_val);
}

toolchange_fsm_state_t changerfsm_get_state(void)
{
  return(toolchange_fsm_state);
}

