void modecon_set_automode(bool_t);

void modecon_set_hosttoolrequest(uint8_t);
void modecon_set_usertoolrequest(uint8_t);

void modecon_init(void);
void modecon_run_toolselection(void);
void modecon_run_motorselection(void);

uint8_t modecon_get_tool_request(void);
toolchanger_mode_t modecon_get_mode(void);

void modecon_set_testmode(bool_t);
void modecon_set_testmotorcontrol(processing_motor_request_t);
void modecon_set_normalmotorcontrol(processing_motor_request_t);
processing_motor_request_t modecon_get_motorcontrol(void);