# README #

Welcome to my project to allow control of the Emco Compact 5 lathe tool changer from a PC running Mach3 (or other) without the need for any proprietary electronics.

### What is this repository for? ###

* The is the source code for an Arduino powered controller for the EMCO Compact 5 Lathe Auto tool changer
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Hardware: 
    * Arduino (Mega 2560 or other!)
    * Omron E6CP Absolute rotary encoder (or other!)
    * Lascar DMX973B LCD (or other!)
    * 24V power supply for ATC motor
    * EMCO Tool changer
* Configuration:
    * Requires board.h configuring for number of tools, encoder position, board pinning etc.
    * Requires Mach3 configuring with tool change macro (To be written)
* Dependencies
    * Arduino development environment
    * Visual Studio 2010
    * CppUTest (unit test framework)
* How to run tests
    * Open Test project, build and execute.
* Deployment instructions
    * Compile and download using arduino environment

### Contribution guidelines ###

* Writing tests
    * TDD principles apply - Red, Green, Refactor!
* Code review
    * None yet!
* Other guidelines
    * None yet!

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact